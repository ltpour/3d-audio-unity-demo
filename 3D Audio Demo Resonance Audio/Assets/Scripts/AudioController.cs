﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    [System.Serializable]
    public class AudioSourceGroup
    {
        public AudioSource[] audioSources;
	public int Length { get { return audioSources.Length; } }
	public AudioSource this[int key]
	{
	    get { { return audioSources[key]; } }
	    set { { audioSources[key] = value; } }
	}
    }
    
    public AudioSourceGroup[] audioSourceGroups;
    private MeshRenderer[][] meshRenderers;
    public Material audioSourceOffMaterial;
    private Material[][] audioSourceOnMaterials;
    [Range (0.2f, 1.0f)] public float[] audioSourceOnVolumes;
    
    void Start()
    {
	meshRenderers = new MeshRenderer[audioSourceGroups.Length][];
	audioSourceOnMaterials = new Material[audioSourceGroups.Length][];
	for (int i = 0; i < audioSourceGroups.Length; i++)
	{
	    meshRenderers[i] = new MeshRenderer[audioSourceGroups[i].Length];
	    audioSourceOnMaterials[i] = new Material[audioSourceGroups[i].Length];
	    for (int j = 0; j < audioSourceGroups[i].Length; j++)
	    {
		MeshRenderer r = audioSourceGroups[i][j].gameObject.GetComponent<MeshRenderer>();
		meshRenderers[i][j] = r;
		audioSourceOnMaterials[i][j] = r.materials[0];
		if (audioSourceGroups[i][j].volume < 0.1f)
		{
		    Material[] rmats = r.materials;
		    rmats[0] = audioSourceOffMaterial;
		    r.materials = rmats;
		}
	    }
	}
    }

    void Update()
    {
	if (Input.GetButtonDown("Fire1"))
	{
	    int audioSourceGroupToSwitch = 0;
	    float minDistance = Vector3.Distance(audioSourceGroups[0][0].transform.position, transform.position);
	    for (int i = 0; i < audioSourceGroups.Length; i++)
	    {
		for (int j = 0; j < audioSourceGroups[i].Length; j++)
		{
		    float d = Vector3.Distance(audioSourceGroups[i][j].transform.position, transform.position);
		    if (d < minDistance)
		    {
			audioSourceGroupToSwitch = i;
			minDistance = d;			
		    }
		}
	    }
	    if (audioSourceGroups[audioSourceGroupToSwitch][0].volume < 0.1f)
	    {
		for (int i = 0; i < audioSourceGroups[audioSourceGroupToSwitch].Length; i++)
		{
		    audioSourceGroups[audioSourceGroupToSwitch][i].volume = audioSourceOnVolumes[audioSourceGroupToSwitch];
		    MeshRenderer r = audioSourceGroups[audioSourceGroupToSwitch][i].gameObject.GetComponent<MeshRenderer>();
		    Material[] rmats = r.materials;
		    rmats[0] = audioSourceOnMaterials[audioSourceGroupToSwitch][i];
		    r.materials = rmats;
		}
	    }
	    else
	    {
		for (int i = 0; i < audioSourceGroups[audioSourceGroupToSwitch].Length; i++)
		{
		    audioSourceGroups[audioSourceGroupToSwitch][i].volume = 0.0f;
		    MeshRenderer r = audioSourceGroups[audioSourceGroupToSwitch][i].gameObject.GetComponent<MeshRenderer>();
		    Material[] rmats = r.materials;
		    rmats[0] = audioSourceOffMaterial;
		    r.materials = rmats;
		}		
	    }
	}
    }
}
