﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbisonicRecorder : MonoBehaviour
{
    void Start()
    {
	ResonanceAudio.StartRecording();
    }

    void OnApplicationQuit()
    {
	string outPath = Application.dataPath + "/out.wav";
        ResonanceAudio.StopRecordingAndSaveToFile(outPath, false);
	Debug.Log("Ambisonic output recorded to " + outPath);
    }
}
