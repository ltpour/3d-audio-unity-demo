﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Axis {x, y, z}
public enum Direction {forward, backward}

public class LinearMotion : MonoBehaviour
{
    public Axis axis;
    public Direction direction;
    public float minPosition;
    public float maxPosition;
    public float speed = 5.0f;

    void MoveBackward()
    {
	switch (axis)
	{
	    case Axis.x:
		if (transform.position.x > minPosition)
		{
		    transform.Translate(Vector3.left * speed * Time.deltaTime);
		}
		else
		{
		    direction = Direction.forward;
		}
		break;
	    case Axis.y:
		if (transform.position.y > minPosition)
		{
		    transform.Translate(Vector3.down * speed * Time.deltaTime);
		}
		else
		{
		    direction = Direction.forward;
		}
		break;
	    case Axis.z:
		if (transform.position.z > minPosition)
		{
		    transform.Translate(Vector3.back * speed * Time.deltaTime);
		}
		else
		{
		    direction = Direction.forward;
		}
		break;
	}	
    }
    
    void MoveForward()
    {
	switch (axis)
	{
	    case Axis.x:
		if (transform.position.x < maxPosition)
		{
		    transform.Translate(Vector3.right * speed * Time.deltaTime);
		}
		else
		{
		    direction = Direction.backward;
		}
		break;
	    case Axis.y:
		if (transform.position.y < maxPosition)
		{
		    transform.Translate(Vector3.up * speed * Time.deltaTime);
		}
		else
		{
		    direction = Direction.backward;
		}
		break;
	    case Axis.z:
		if (transform.position.z < maxPosition)
		{
		    transform.Translate(Vector3.forward * speed * Time.deltaTime);
		}
		else
		{
		    direction = Direction.backward;
		}
		break;
	}	
    }
    
    void Update()
    {
	switch (direction)
	{
	    case Direction.forward:
		MoveForward();
		break;
	    case Direction.backward:
		MoveBackward();
		break;
	}
    }
}
